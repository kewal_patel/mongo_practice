from django.conf.urls import url, include
from views import index, login

urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^$', index, name='index'),
    url(r'^login/$',login, name='login'),
    url(r'^register/$',login, name='register'),
    url(r'^forgot-password/$',login, name='forgot-password'),
]