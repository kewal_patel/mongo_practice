from django.shortcuts import render

# Create your views here.
def index(request):
    # employee = Employee.objects.create(
    #     email="pedro.kong@company.com",
    #     first_name="Pedro",
    #     last_name="Kong"
    # )
    # employee.save()
    return render(request, 'core/index.html', {})

def login(request):
	return render(request, 'core/login.html', {})

def register(request):
	return render(request, 'core/register.html', {})

def forgot_password(request):
	return render(request, 'core/forgot_password.html', {})